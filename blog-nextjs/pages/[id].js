import Link from "next/link";
import styles from '../styles/Home.module.css'

export default function Post({ post }) {
  return (
    <div className={styles.container}>
      <Link href={'/'}>Back to Home</Link>
      <h2>{post.attributes.Title}</h2>
      <p>{post.attributes.Content}</p>
    </div>
  );
}

export const getStaticPaths = async () => {
  const res = await fetch("http://localhost:1337/api/posts")
  const data = await res.json()
  const posts = await data.data

  const paths = posts.map(post => ({
    params: { id: post.id.toString() }
  }))

  return {
    paths,
    fallback: true
  }
}

export const getStaticProps = async ({ params }) => {
  const { id } = params

  const res = await fetch(`http://localhost:1337/api/posts/${id}`)
  const data = await res.json()
  const post = data.data

  return {
    props: {
      post
    }
  }
}