import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home({ posts }) {
  return <div className={styles.container}>
    {posts && posts.map((post) => (
      <div key={post.id}>
        <Link href={`/${post.id}`} >
          <a><h2>{post.attributes.Title}</h2></a>
        </Link>
      </div>
    ))}
  </div>

}

export async function getStaticProps() {
  const res = await fetch("http://localhost:1337/api/posts")
  const data = await res.json()
  const posts = await data.data

  return {
    props: {
      posts
    }
  }
}
